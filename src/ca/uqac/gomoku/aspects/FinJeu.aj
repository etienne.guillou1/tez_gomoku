package ca.uqac.gomoku.aspects;

import java.util.List;

import ca.uqac.gomoku.core.Player;
import ca.uqac.gomoku.core.model.Grid;
import ca.uqac.gomoku.core.model.Spot;
import ca.uqac.gomoku.ui.App;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public aspect FinJeu {
	
	/* Variables */
	private List<Spot> winningStones;
	private boolean gameOver = false, uiModified = false;
	private Grid grid;
	private Canvas playGround;
	
	/* Pointcuts */
	pointcut getGrid(Grid grid) : set(Grid App.gameGrid) && args(grid);
	pointcut getCanvas(Canvas playGround): set(Canvas App.playGround) && args(playGround);
	pointcut getWinningStones(List<Spot> stones): set(List<Spot> Grid.winningStones) && args(stones);

	// Recuperation de la grille
	after(Grid grid): getGrid(grid) {
		this.grid = grid;
	}
	
	// Recuperation du canvas
	after(Canvas playGround): getCanvas(playGround) {
		this.playGround = playGround;
	}
	
	// Recuperation des pierres gagnantes
	after(List<Spot> stones): getWinningStones(stones) {
		if(stones.size() >= 5 && !gameOver) winningStones = stones;
	}
	
	// Lorsque la partie se termine
	void around(Player winner): call(* gameOver(Player)) && args(winner) {
		if(!gameOver) {
			gameOver = true;

			// Pas de setter sur color dans Player, donc on fait un nouveau joueur pour changer la couleur des pierres
			Player newPlayer = new Player("Winner", Color.AQUA);
			
			for(Spot stone : winningStones) {
				grid.placeStone(stone.x, stone.y, newPlayer);
			}
			
			uiModified = true;
			
			// Affichage du gagnant : on ecrit sur le canvas du jeu
			GraphicsContext gc = this.playGround.getGraphicsContext2D();
			gc.setFill(Color.RED);
			gc.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
			gc.fillText("Partie termin�e, victoire de " + winner.getName(), 60, 50);
			
		}
	}
	
	// On empeche aux joueurs de jouer une fois que la partie est terminee
	void around(): call(* stonePlaced(Spot)) {
		if(!uiModified || !gameOver)
			proceed();
	}

	
}
