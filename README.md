# TEZ_Gomoku

8INF957 : Programmation objet avancée
TP3 - Gomoku avec AspectJ

Université du Québec A Chicoutimi (UQAC)

## Auteurs : 
    - Chevallier Zoé
    - Denis Thibault
    - Guillou Etienne 
    
## Date : 
    - 22/NOV/2018
    
## Langage : 
    - Java
